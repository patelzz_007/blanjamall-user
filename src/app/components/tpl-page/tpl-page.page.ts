
import { Component, OnInit, Input, Output,ViewChild, ContentChild, TemplateRef, EventEmitter } from '@angular/core';
import { TemplatePageDirective } from 'src/app/directives';
import { ActionSheetController, NavController, MenuController, IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';
import { WidgetService } from 'src/app/services/common';
import { FunctionsService } from '../../functions.service';
import { DataService} from '../../data.service';
import { ActivatedRoute } from '@angular/router';
import { HomeTab, NotificationsCard, Notification, Product, Review, Cart, User, Orders} from 'src/app/models/base/products';




@Component({
  selector: 'app-tpl-page',
  templateUrl: './tpl-page.page.html',
  styleUrls: ['./tpl-page.page.scss'],
})
export class TemplatePage implements OnInit {
  @ContentChild(TemplatePageDirective, { static: true, read: TemplateRef }) pageTpl: TemplateRef<any>;
  @Input() title: string;
  @Input() isBack: boolean;
  @Output() segmentSlide = new EventEmitter();

  // @ViewChild('Slides') slides: IonSlides;
  @ViewChild('Slides', { static: true }) slides: IonSlides;
  segment = '';
  index = 0;
  data: Array<HomeTab> = [];
  sponsored: Array<Product> = [];
  product_data_1: Array<Product> = [];
  product_data_2: Array<Product> = [];
  product_data_3: Array<Product> = [];
  product_data_4: Array<Product> = [];
  product_data_5: Array<Product> = [];
  slideOpts = {
    effect: 'flip',
    zoom: false
  };

  constructor(
    private actionSheetController: ActionSheetController,
    private router: Router,
    private nav: NavController,
    private widgets: WidgetService,
    private activatedRoute: ActivatedRoute,
    private menuCtrl: MenuController,
    private fun: FunctionsService,
    private dataService: DataService
  ) {
    this.data = dataService.tabs;
    this.sponsored = dataService.sponsored;
    this.product_data_1 = dataService.products_1;
    this.product_data_2 = dataService.products_2;
    this.product_data_3 = dataService.products_3;
    this.product_data_4 = dataService.products_4;
    this.product_data_5 = dataService.products_5;
    const d = this.activatedRoute.snapshot.paramMap.get('id');
    console.log("WHAT IS THIS : ", d)
    if (d) {
      this.segment = this.data[parseInt(d, 10)].title;
    } else {
      this.segment = this.data[0].title;
    }
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(true, 'start');
    this.menuCtrl.enable(true, 'end');
  }

  seg(event) {
    this.segment = event.detail.value;
    console.log("seg => ",this.segment)
  }

  drag() {
    let distanceToScroll = 0;
    for (let index in this.data) {
      if (parseInt(index) < this.index) {
        distanceToScroll = distanceToScroll + document.getElementById('seg_' + index).offsetWidth + 24;
      }
    }
    document.getElementById('dag').scrollLeft = distanceToScroll;
  }

  preventDefault(e) {
    e.preventDefault();
  }

  // @Input 
  // set segment(){}

  async change() {
    await this.slides.getActiveIndex().then(data => this.index = data);
    this.segment = this.data[this.index].title;
    this.drag();
  }

  side_open() {
    this.menuCtrl.toggle('end');
  }

  update(i) {
    console.log(i);
    this.segmentSlide.emit(i);
    // this.slides.slideTo(i).then((res) => console.log('responseSlideTo', res));
  }

  
}
