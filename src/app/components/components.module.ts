import { FormsModule } from '@angular/forms';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuccessmodalPage } from './modal-success/modal-success.page';
import { IonicModule } from '@ionic/angular';
import { TemplateMenu } from './tpl-main/tpl-menu.page';
import { TemplateForm } from './tpl-form/tpl-form.page';
import { LoadingModal } from './modal-loading/modal-loading.component';
import { ExpandableComponent } from './expandable/expandable.component';
import { TemplateBody } from './tpl-body/tpl-body.page';
import { TemplatePage } from './tpl-page/tpl-page.page';
import { ProductListComponent } from './product-list/product-list.component';

const components = [
    SuccessmodalPage,
    LoadingModal,
    TemplateMenu,
    TemplateForm,
    TemplatePage,
    TemplateBody,
    ExpandableComponent,
    ProductListComponent
]

@NgModule({
  declarations: components,
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports:components,
})
export class AppComponentsModule { 
  static forRoot() {
      return {
          ngModule: AppComponentsModule,
          providers: []
      }
  } 
}
