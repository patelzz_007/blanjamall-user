// import { StorageService } from 'src/app/services/storage/storage';
import { Component, OnInit, Input, ContentChild, TemplateRef } from '@angular/core';
import { TemplateBodyDirective } from 'src/app/directives';
import { ActionSheetController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
// import { User, Photo } from 'src/app/models/base/auth';
// import { ProfileApi } from 'src/app/services/api';


@Component({
  selector: 'app-tpl-body',
  templateUrl: './tpl-body.page.html',
  styleUrls: ['./tpl-body.page.scss'],
})
export class TemplateBody implements OnInit {
  @ContentChild(TemplateBodyDirective, { static: true, read: TemplateRef }) bodyTpl: TemplateRef<any>;
  @Input() title: string;
  @Input() isBack: boolean;

  constructor(
    private actionSheetController: ActionSheetController,
    private router: Router,
    private nav: NavController,
  ) { }

  ngOnInit() {  }



  goBack() {
    this.nav.back();
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Pilihan',
      cssClass: 'my-custom-class',
      buttons: [
        {
          text: 'Wallet',
          icon: 'list-outline',
          handler: this.navigate.bind(this, "wallet")
        },
        {
          text: 'Message',
          icon: 'bulb-outline',
          handler: this.navigate.bind(this, "message")
        }, 
        {
          text: 'Logout',
          icon: 'log-out-outline',
          handler: this.logout.bind(this)
        }
      ]
    });
    await actionSheet.present();
  }

  logout(route){
    this.router.navigate([""]);
  }

  navigate(route){
    try{
      this.router.navigate([route]);
    } catch (e) {
      console.log(`Add "${route}" in routing`);
    }
  }
}
