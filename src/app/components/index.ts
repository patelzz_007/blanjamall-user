export { ExpandableComponent } from './expandable/expandable.component';
export { SuccessmodalPage } from "./modal-success/modal-success.page";
export { LoadingModal } from "./modal-loading/modal-loading.component";
export { TemplateMenu } from "./tpl-main/tpl-menu.page";
export { TemplateForm } from "./tpl-form/tpl-form.page";
export { TemplateBody } from './tpl-body/tpl-body.page';
