import { Component, OnInit, Input, Output, ContentChild, TemplateRef } from '@angular/core';

import { TemplateFormDirective } from 'src/app/directives';

@Component({
  selector: 'app-tpl-form',
  templateUrl: './tpl-form.page.html',
  styleUrls: ['./tpl-form.page.scss'],
})
export class TemplateForm implements OnInit {
  @ContentChild(TemplateFormDirective, { static: true, read: TemplateRef }) tpl: TemplateRef<any>;
  @Input() title: string;

  constructor(
  ) { }

  ngOnInit() {
  }
}
