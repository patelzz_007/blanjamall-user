import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-modal-success',
  templateUrl: './modal-success.page.html',
  styleUrls: ['./modal-success.page.scss'],
})
export class SuccessmodalPage implements OnInit {
  @Input() toPage: string;
  @Input() text: string;
  constructor(
    private modalCtrl: ModalController,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    setTimeout(() => {
      this.page();
      setTimeout(() => {
        this.dismiss();
      }, 500);
    }, 1500);
  }

  contentBody() {
    return "login";
  }

  contentFooter() {
    return "login-footer";
  }

  page() {
    if (this.toPage) {
      this.navCtrl.navigateRoot(this.toPage);
      return;
    }
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

}
