import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../models/base/products';
import { FunctionsService }  from 'src/app/functions.service'
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-productlist',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {

  @Input() recieved_data: Array<Product>;


  constructor(
    private fun: FunctionsService,
     private nav: NavController
  ) { }

  ngOnInit() {
    console.log("COMPONENT RECEIVED DATA",this.recieved_data)
  }

  open(data){
    this.fun.update(data);
    console.log("WHAT ARE YOU : ", data)
    this.nav.navigateForward('/main/productdetail');
  }

}
