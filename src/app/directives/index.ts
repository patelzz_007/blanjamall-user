export { TemplateMenuDirective } from './tpl/tpl-menu.directive';
export { TemplateFormDirective } from './tpl/tpl-form.directive';
export { TemplateBodyDirective } from './tpl/tpl-body.directive';
export { TemplatePageDirective } from './tpl/tpl-page.directive';