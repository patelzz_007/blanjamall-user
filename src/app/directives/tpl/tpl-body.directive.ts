import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[tplBody]'
})
export class TemplateBodyDirective {

  constructor(public tpl: TemplateRef<any>) { }

}