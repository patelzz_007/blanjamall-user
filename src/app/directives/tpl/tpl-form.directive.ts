import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[tplForm]'
})
export class TemplateFormDirective {

  constructor(public tpl: TemplateRef<any>) { }

}