import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[tplMenu]'
})
export class TemplateMenuDirective {

  constructor(public tpl: TemplateRef<any>) { }

}