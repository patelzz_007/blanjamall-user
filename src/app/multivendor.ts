import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppComponentsModule } from './components/components.module';
import { AppDirectivesModule } from './directives/directives.module';

@NgModule({
  declarations: [],
  imports: [
    AppComponentsModule,
    AppDirectivesModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  exports:[
  ],
})
export class multivendor {
    static forRoot() {
        return {
            ngModule: multivendor,
            providers: []
        }
    }
 }
