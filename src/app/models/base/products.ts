import { Injectable } from '@angular/core';

export interface HomeTab {
    title: string
  };
  
  export interface NotificationsCard {
    image: string,
    title: string,
    time: number
  }
  
  export interface Notification {
    all: Array<NotificationsCard>,
    deals: Array<NotificationsCard>,
    orders: Array<NotificationsCard>,
    others: Array<NotificationsCard>
  }
  
  export interface Product {
    name: string,
    image: Array<string>,
    size: string,
    color: string,
    cost_price: number,
    discount: number,
    offer: boolean,
    stock: number,
    description: string,
    currency: string,
    bought: number,
    shipping: number,
    rating: number,
    rating_count: number,
    store_rate: number,
    store_rating: number,
    store_rating_count: number,
    sold_by: string,
    specs: string,
    reviews: Array<Review>,
    store_reviews: Array<Review>,
    sizing: {
      small: number,
      okay: number,
      large: number
    },
    buyer_guarantee: string,
    sponsored: Array<Product>
  }
  export interface Review {
    image: string,
    name: string,
    comment: string,
    rating: number,
    images: Array<string>
  }
  export interface Cart {
    product: Product,
    quantity: number
  }
  
  export interface User {
    fname: string,
    lname: string,
    email: string,
    address: Array<Address>,
    billing: Array<any>,
    uid: string,
    did: string,
    aid: string
  }
  
  export interface Address {
    first_name: string,
    last_name: string,
    address_line_1: string,
    address_line_2: string,
    country: string,
    state: string,
    city: string,
    zipcode: number,
    phone_number: number
  }
  
  export interface Orders {
    product: Product,
    order_date: Date,
    id: string,
    amount: number,
    delivery_date: Date,
    status: string,
    billing_address: Address,
    shipping_address: Address,
    tax: number
  }