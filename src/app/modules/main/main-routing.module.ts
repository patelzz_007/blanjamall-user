import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainPage } from './main.page';

export const routes: Routes = [
  {
    path: '',
    component: MainPage,
    children: [

      {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'productdetail',
        loadChildren: () => import('./pages/productdetail/productdetail.module').then( m => m.ProductdetailPageModule)
      },
      {
        path: 'cart',
        loadChildren: () => import('./pages/cart/cart.module').then( m => m.CartPageModule)
      },
      {
        path: 'checkout',
        loadChildren: () => import('./pages/checkout/checkout.module').then( m => m.CheckoutPageModule)
      }
    ]
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainPageRoutingModule {}
