
import { Component, OnInit } from '@angular/core';
import { WidgetService } from 'src/app/services/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  passwordtype: string = 'password';
  passwordcolor: string  = 'dark';
  password :boolean = false;

  // form: Login;

  constructor(
    private widgets: WidgetService,
    )
  {
    // this.initForm();
  }

  // initForm() {
  //   this.form = {
  //     icno: "770525075269",
  //     password: "hrmis2020"
  //   };
  // }

  async shownpassword(){
    if(this.password){
      this.password = false;
      this.passwordtype = 'password';
      this.passwordcolor = 'dark';
    }
    else
    {
      this.password = true;
      this.passwordtype = 'text';
      this.passwordcolor = '';
    }
  }

  ngOnInit() {
  }

  async login(){
    const loading = await this.widgets.loading();
    try{
      loading.present();
      this.widgets.showSuccessModal('main/home', 'Success');
      loading.dismiss();
    }
    catch (e) { 
      console.error(e);
      loading.dismiss();  
  }

  }
}
