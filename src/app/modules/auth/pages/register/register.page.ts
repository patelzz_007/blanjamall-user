import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  passwordtype: string = 'password';
  cpasswordtype: string = 'password';
  password: boolean = false;
  cpassword: string;
  // form: Register;

  constructor(
    private navCtrl: NavController){

  }
  

  back() {
    this.navCtrl.pop();
  }

  ngOnInit() {
  }



  async shownpassword(passwordtype) {
    if (this[passwordtype] == 'text') {
      this[passwordtype] = 'password';
      return;
    }

    this[passwordtype] = 'text';
  }


}
